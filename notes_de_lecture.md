> https://www.liglab.fr/fr/evenements/keynote-speeches/pierre-yves-longaretti-global-environmental-collapse-risks-is-digital

du coup j'ai regardé et j'ai mis mes commentaires ici


01:48 Limit of the growth, 2004 ?????????? je dirais 1972 moi.
04:34 On a pas de problème de surpopulation mais de modèle de surconsommation

      Pour faire vite: le problème c'est le mode de vie occidental,
      pas la surpopulation

    * une étude récente montre que même sans faire appel à la
      collapsologie, la transition démographique aurait permis de
      stabiliser la population vers 2050
    * plusieurs études à croiser pour montrer que
      * c'est l'économie de marché qui est l'origine du problème
        * production et acheminement organisé autour de la seule
          rentabilité économique
        * stimulation de la demande par l'explosion d'offres de moins
          en moins durables (fragilité, obsolescence programmée)
      * cette économie profite à une frange très réduite de la
        population

26:20: good point!
    * unlimited desires
    * competition
        => stress desire strategy

29:00 society complexity ? 

    * étant ingénieur, ca me parle mais je ne crois pas que ce soit
      le principal probleme! cf. point précédent

    * je crois au principe de dilbert: décomplexifier, c'est admettre
      l'inutilité des élites et de la compétition

34:00 super intéressant !!! si on remplace complexity par rentabilité

40:00 les memes graphiques que le shift

    * j'aimerais mettre la main sur
        * les sources exactes
        * les données brutes
    * meme question pour la mixité énergétique

41:19 bon résumé des impacts écologiques

43:40
    * illusion of demat est le fruit d'un discours
    * indirect effects et hidden costs: audition senat giraud!!

48:29 *super intéressant*
    Transition numérique vs ecologique 😍
    Colapse is on its way: imagine solutions w or w/o numerics

NB: je serais super intéressé par voir a quel point virer la video du
gars déduirait le poids de la video.
